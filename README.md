# Welcome to not!

Not is a terminal-based note-taking app.
It handles your notes in a markdown format,
so it's possible to sync the notes between your devices.

## Building

not is written in Rust, so you'll need to grab a
[Rust installation](https://www.rust-lang.org/) in order to compile it.
not compiles with Rust 1.34.0 (stable) or newer. In general, not tracks
the latest stable release of the Rust compiler.

To build not:

```
$ git clone https://gitlab.com/joao-vitor-sr/not
$ cd not
$ cargo build --release
$ ./target/release/not version
0.1.0
```
